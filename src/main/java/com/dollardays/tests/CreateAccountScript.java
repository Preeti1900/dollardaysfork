package com.dollardays.tests;

import java.io.IOException;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentTest;
import com.dollardays.common.BaseTest;
import com.dollardays.common.CommonUtilities;
import com.dollardays.common.ReadExcel;
import com.dollardays.weblocators.CreateAccountPage;

public class CreateAccountScript extends BaseTest {
	private static final String Screenshots = null;
	private CreateAccountPage createAccountPage = null;
	ExtentTest extentTest;

	@BeforeMethod
	public void setup() throws IOException {
		extentReportSetUp();
		initialization();
		ReadExcel.cleanUpFile(CommonUtilities.getPropertyValue("testreportfilepath"));
		createAccountPage = new CreateAccountPage(driver);

	}

	@Test()
	public void Tc_01_ClickSignUpButton() throws InterruptedException, IOException {
		extentTest = this.getExtentReport().createTest("Test Case 1", "Click sign up button");
		
		this.createAccountPage.clickSignInButton();
		this.createAccountPage.clickCreateAccount();
		this.createAccountPage.sendSignUpInformation();
		boolean isTestCaseSuccess = this.createAccountPage.clickSignUpButton();
 		
		ReadExcel.writeresult(CommonUtilities.getPropertyValue("testreportfilepath"), 4, 0, 0, "Signup", false);
		ReadExcel.writeresult(CommonUtilities.getPropertyValue("testreportfilepath"), 4, 1, 0, "Test", true);
		ReadExcel.writeresult(CommonUtilities.getPropertyValue("testreportfilepath"), 4, 2, 0, "Pass", true);

		CommonUtilities.captureScreenShot(driver, "Account generated", "Tc_01_ClickSignUpButton");

	}

	@AfterMethod
	public void terminateBrowser() throws InterruptedException {

		this.getExtentReport().flush();
		termination();

	}

}