//Preeti's comment
package com.dollardays.weblocators;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.dollardays.common.CommonUtilities;
import com.dollardays.common.ReadExcel;

public class CreateAccountPage {
	@FindBy(xpath = "//li[@class='dropdown']//a[@class='dropdown-toggle']")
	private WebElement signInButton;
	@FindBy(xpath = "//a[contains(text(),'Create account')]")
	private WebElement createAccount;
	@FindBy(xpath = "//input[@id='ctl00_cphContent_txtFName']")
	private WebElement userFirstNameTextField;
	@FindBy(xpath = "//input[@id='ctl00_cphContent_txtLName']")
	private WebElement userLastNameTextField;
	@FindBy(xpath = "//input[@id='ctl00_cphContent_txtClientEmail']")
	private WebElement userEmailTextField;
	@FindBy(xpath = "//input[@id='ctl00_cphContent_txtPhoneMain']")
	private WebElement userPhoneTextField;
	@FindBy(xpath = "//input[@id='ctl00_cphContent_txtPassword']")
	private WebElement userPasswordField;
	@FindBy(xpath = "//input[@id='ctl00_cphContent_txtPasswordConfirm']")
	private WebElement userConfirmPasswordField;
	@FindBy(xpath = "//input[@id='ctl00_cphContent_btnRegister']")
	private WebElement userSignUpButton;
	@FindBy(xpath = "//h4[contains(text(),'Welcome to DollarDays!')]")
	private WebElement successfulRegistrationMessage;
	
	
	
	
	public CreateAccountPage(WebDriver driver) {
		
		PageFactory.initElements(driver, this);
	}
	
	public void clickSignInButton() {

		signInButton.click();
	}

	public void clickCreateAccount() {

		createAccount.click();
	}
	public void sendSignUpInformation() throws IOException {

		userFirstNameTextField.sendKeys(ReadExcel.getData(CommonUtilities.getPropertyValue("testdatapath"), 1, 1, 0));
		userLastNameTextField.sendKeys(ReadExcel.getData(CommonUtilities.getPropertyValue("testdatapath"), 1, 1, 1));
		userEmailTextField.sendKeys((ReadExcel.getData(CommonUtilities.getPropertyValue("testdatapath"), 1, 1, 2)));
		userPhoneTextField.sendKeys((ReadExcel.getData(CommonUtilities.getPropertyValue("testdatapath"), 1, 1, 3)));
		userPasswordField.sendKeys((ReadExcel.getData(CommonUtilities.getPropertyValue("testdatapath"), 1, 1, 4)));
		userConfirmPasswordField.sendKeys((ReadExcel.getData(CommonUtilities.getPropertyValue("testdatapath"), 1, 1, 5)));
	}
	
	public boolean clickSignUpButton() {

		userSignUpButton.click();
		return successfulRegistrationMessage.isDisplayed();
	}
}
